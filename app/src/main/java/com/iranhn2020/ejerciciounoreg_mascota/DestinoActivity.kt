package com.iranhn2020.ejerciciounoreg_mascota

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.iranhn2020.ejerciciounoreg_mascota.databinding.ActivityDestinoBinding


class DestinoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDestinoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDestinoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle : Bundle? = intent.extras
        bundle?.let {bundleLibriDeNull ->

            val    nombre = bundleLibriDeNull.getString("Key_nombre","" )
            val      edad = bundleLibriDeNull.getString("key_edad","0" )
            val    genero = bundleLibriDeNull.getString("key_genero","" )
            val vacunarab = bundleLibriDeNull.getString("key_vacunarab", "" )
            val vacunadis = bundleLibriDeNull.getString("key_vacunadis", "" )
            val vacunapar = bundleLibriDeNull.getString("key_vacunapar", "" )
            val vacunahep = bundleLibriDeNull.getString("key_vacunahep", "" )
            val vacunalep = bundleLibriDeNull.getString("key_vacunalep", "" )


           binding.tvNombre.text = "Nombre: $nombre"
           binding.tvEdad.text = "Edad: $edad"
           binding.tvGenero.text = "Mascota $genero"
           binding.tvVacunarab.text = "$vacunarab"
           binding.tvVacunadis.text = "$vacunadis"
           binding.tvVacunapar.text = "$vacunapar"
           binding.tvVacunahep.text = "$vacunahep"
           binding.tvVacunalep.text = "$vacunalep"



            val imagen = binding.imgMascota

            var drawableResource  = when (binding.tvGenero.text.toString()) {
                "Mascota Perro" -> R.drawable.perro
                "Mascota Gato"-> R.drawable.gato
                "Mascota Conejo"-> R.drawable.conejo
                else -> R.drawable.interr
            }

            imagen.setImageResource(drawableResource)
            println("$drawableResource")
        }



    }
}