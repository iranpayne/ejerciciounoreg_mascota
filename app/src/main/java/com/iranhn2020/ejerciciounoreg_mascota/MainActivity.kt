package com.iranhn2020.ejerciciounoreg_mascota


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.iranhn2020.ejerciciounoreg_mascota.databinding.ActivityMainBinding
// by Iran Payne
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegistrar.setOnClickListener {
            var nombre = binding.edtNombre.text.toString()
            var edad = binding.edtEdad.text.toString()


            if (nombre.isEmpty()) {
                Toast.makeText(this, "Debe ingresar sus nombres", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (edad.isEmpty()) {
                Toast.makeText(this, "Debe ingresar la edad", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!binding.rdbConejo.isChecked and !binding.rdbPerro.isChecked and !binding.rdbGato.isChecked) {
                Toast.makeText(this, "Debe selecionar una de las mascota", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val check = true
            var genero = when (check) {
                binding.rdbConejo.isChecked -> "Conejo"
                binding.rdbPerro.isChecked -> "Perro"
                binding.rdbGato.isChecked -> "Gato"
                else -> ""
            }

            var vacunarab  = if(binding.chkVacunaRab.isChecked) "Vacuna contra la rabia" else "Vacuna N/A"
            var vacunadis  = if(binding.chkVacunaDis.isChecked) "Vacuna contra el distemper" else "Vacuna N/A"
            var vacunapar  = if(binding.chkVacunaPar.isChecked) "Vacuna contra parvovirus" else "Vacuna N/A"
            var vacunahep  = if(binding.chkVacunaHep.isChecked) "Vacuna contra la hepatitis" else "Vacuna N/A"
            var vacunalep  = if(binding.chkVacunaLep.isChecked) "Vacuna contra la leptospirosis" else "Vacuna N/A"


            val bundle = Bundle()
            bundle.apply {
                putString("Key_nombre", nombre)
                putString("key_edad", edad)
                putString("key_genero", genero)
                putString("key_vacunarab", vacunarab)
                putString("key_vacunadis", vacunadis)
                putString("key_vacunapar", vacunapar)
                putString("key_vacunahep", vacunahep)
                putString("key_vacunalep", vacunalep)

            }

            val intent = Intent(this,DestinoActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}